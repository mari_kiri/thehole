#!/bin/sh

export ZIPNAME=latest_exe_TheHole.zip

zip -r $ZIPNAME *.dll *.exe *.txt opencmd.bat docs/ \
	clsave/config.json \
	clsave/pub/user.json \
	clsave/pub/controls.json \
	clsave/vol/dummy \
	svsave/pub/dummy \
	svsave/vol/dummy \
	pkg/sparkle/launch/ \
	pkg/sparkle/lib/ \
	pkg/sparkle/gfx/ \
	#

