The Hole
Copyright (C) 2012-2013, Iceball contributors
Copyright (C) 2014, Team Sparkle

*** LICENCE THINGS ***
lib_sdlkey.lua is taken from SDL and is LGPLv2.1-licensed.
The rest of the Lua side is MIT-licensed.
The game engine ("Iceball") is GPLv3-licensed.
Assets are CC3-BY-SA-licensed. Please see here for more info: http://creativecommons.org/licenses/by-sa/3.0/

*** RUNNING THE GAME ***
Run the executable called "TheHole".
You may need to build it first.
If you're running this on a VPS, you can build a dedicated server. You'll still need sackit, but you can drop the SDL dependency.

*** BUILDING ***
There are several makefiles. Pick one using the -f flag for whatever make program you are using. You will need GNU Make.
- Makefile: OpenGL renderer for Linux and BSD.
- Makefile.dedi: Dedicated server for Linux and BSD.
- Makefile.mingw: OpenGL renderer for Windows.
- Makefile.osx: OpenGL renderer for Mac OS X.
- Makefile.osx-clang: OpenGL renderer for Mac OS X, using clang to compile. Or something like that.

No, there is no software renderer for this fork. Anyone who could run it sufficiently could run the OpenGL renderer better. Because we want to shove in some nice features without having to maintain a renderer nobody really uses, it's been dropped.

DISCLAIMER: OS X is not properly supported. If you would like to maintain it, please enquire.

Regular libraries you will need are:
- ENet 1.3.x
- Lua 5.1.x
- SDL 1.2.x
- zlib

Do not use newer versions than stated. They won't work.

All the OpenGL stuff is assumed to be on your system already.

Weird libraries you will need are:
- sackit: https://github.com/iamgreaser/sackit

You will need to put sackit.h and libsackit.a in xlibinc/.

Note, if you are using Windows, or making a Windows build, everything needs to go in winlibs/.

